﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiktionaryTranslator.Models
{
    public class Language
    {
        public string Word { get; set; }
        public List<Definition> Meaning { get; set; }
        public List<string> Synonym { get; set; }
        public List<string> Antonym { get; set; }
        public List<Translation> Translate { get; set; }

        public Language(string word, List<Definition> meaning, List<string> synonym, List<string> antonym, List<Translation> translate)
        {
            Word = word;
            Meaning = meaning;
            Synonym = synonym;
            Antonym = antonym;
            Translate = translate;
        }
    }
}
