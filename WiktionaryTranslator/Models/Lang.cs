﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiktionaryTranslator.Models
{
    public class Lang
    {
        public string LangCode { get; set; }
        public string LangOrigin { get; set; }
        public string LangEnglish { get; set; }

        public Lang(string langCode, string langOrigin, string langEnglish)
        {
            LangCode = langCode;
            LangOrigin = langOrigin;
            LangEnglish = langEnglish;
        }
    }
}
