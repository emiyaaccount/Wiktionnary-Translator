﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiktionaryTranslator.Models
{
    public class Translation
    {
        public string LangCode { get; set; }
        public string TranslateWord { get; set; }

        public Translation(string langCode, string translateWord)
        {
            LangCode = langCode;
            TranslateWord = translateWord;
        }
    }
}
