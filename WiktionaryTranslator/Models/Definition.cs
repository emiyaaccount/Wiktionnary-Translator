﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiktionaryTranslator.Models
{
    public class Definition
    {
        public string Def { get; set; }
        public string Domain { get; set; }
        public string Example { get; set; }

        public Definition(string def, string domain, string example)
        {
            Def = def;
            Domain = domain;
            Example = example;
        }
    }
}
