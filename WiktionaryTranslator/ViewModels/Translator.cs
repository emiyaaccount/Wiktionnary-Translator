﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using WiktionaryTranslator.Models;
using WiktionaryTranslator.Resources;

namespace WiktionaryTranslator.ViewModels
{
    public class Translator : NotifyProp
    {
        public static ObservableCollection<Language> TranslateFromThisLang { get; set; }
        public static ObservableCollection<Language> TranslateToThisLang { get; set; }
        public Lang TranslateFromLang { get; set; }
        public Lang TranslateToLang { get; set; }
        private ICommand changeTranslateFromLang;
        private ICommand changeTranslateToLang;
        private ICommand getTextToTranslate;
        private string textTranslate;
        private ObservableCollection<string> exampleTranslate;
        private ObservableCollection<string> defTranslate;
        private int infoMessageTranslator;
        private int infoMessageExistsXMLFrom;
        private int infoMessageExistsXMLTo;

        public ICommand ChangeTranslateFromLang
        {
            get
            {
                return changeTranslateFromLang = new RelayCommand<Lang>((lang) =>
                {
                    TextTranslate = null;
                    DefTranslate = new ObservableCollection<string>();
                    ExampleTranslate = new ObservableCollection<string>();
                    if (TranslateFromLang.LangCode.Equals(new DataString().DefaultLanguage))
                    {
                        TranslateFromLang = lang;
                        TranslateFromThisLang = MainViewModel.PrimaryLanguage;
                        InfoMessageExistsXMLFrom = 0;
                    }
                    else if (File.Exists(new DataString().WiktionnaryXML + TranslateFromLang.LangCode + new DataString().XML))
                    {
                        TranslateFromLang = lang;
                        TranslateFromThisLang = new TranslationTools().initialyze(new DataString().WiktionnaryXML + TranslateFromLang.LangCode + new DataString().XML);
                        InfoMessageExistsXMLFrom = 0;
                    }
                    else
                    {
                        TranslateFromLang = null;
                        InfoMessageExistsXMLFrom = 30;
                        TranslateFromThisLang = null;
                    }
                });
            }
        }

        public ICommand ChangeTranslateToLang
        {
            get
            {
                return changeTranslateToLang = new RelayCommand<Lang>((lang) =>
                {
                    TextTranslate = null;
                    DefTranslate = new ObservableCollection<string>();
                    ExampleTranslate = new ObservableCollection<string>();
                    if (TranslateToLang.LangCode.Equals(new DataString().DefaultLanguage))
                    {
                        TranslateToLang = lang;
                        TranslateToThisLang = MainViewModel.PrimaryLanguage;
                        InfoMessageExistsXMLTo = 0;
                    }
                    if (File.Exists(new DataString().WiktionnaryXML + TranslateToLang.LangCode + new DataString().XML))
                    {
                        TranslateToLang = lang;
                        TranslateToThisLang = new TranslationTools().initialyze(new DataString().WiktionnaryXML + TranslateToLang.LangCode + new DataString().XML);
                        InfoMessageExistsXMLTo = 0;
                    }
                    else
                    {
                        TranslateToLang = lang;
                        InfoMessageExistsXMLTo = 30;
                        TranslateToThisLang = null;
                    }
                });
            }
        }

        public ICommand GetTextToTranslate
        {
            get
            {
                return getTextToTranslate = new RelayCommand<string>((textToTranslate) =>
                {
                    if (TranslateFromLang != null && TranslateToLang != null)
                    {
                        InfoMessageTranslator = 0;
                        TextTranslate = new TranslationTools().TranslateSentence(TranslateToLang.LangCode, textToTranslate, TranslateFromThisLang);
                        if (TranslateToThisLang!=null)
                        {
                            ExampleTranslate = new TranslationTools().ListOfExampleFromSentence(TextTranslate, TranslateToThisLang);
                        }
                        DefTranslate = new TranslationTools().ListOfDefFromSentence(TextTranslate, TranslateFromThisLang);
                    }
                    else
                    {
                        InfoMessageTranslator = 50;
                        TextTranslate = null;
                        DefTranslate = new ObservableCollection<string>();
                        ExampleTranslate = new ObservableCollection<string>();
                    }
                });
            }
        }

        public string TextTranslate
        {
            get
            {
                return textTranslate;
            }

            set
            {
                textTranslate = value;
                NotifyPropertyChanged();
            }
        }

        public ObservableCollection<string> ExampleTranslate
        {
            get
            {
                return exampleTranslate;
            }

            set
            {
                exampleTranslate = value;
                NotifyPropertyChanged();
            }
        }

        public int InfoMessageTranslator
        {
            get
            {
                return infoMessageTranslator;
            }

            set
            {
                infoMessageTranslator = value;
                NotifyPropertyChanged();
            }
        }

        public int InfoMessageExistsXMLFrom
        {
            get
            {
                return infoMessageExistsXMLFrom;
            }

            set
            {
                infoMessageExistsXMLFrom = value;
                NotifyPropertyChanged();
            }
        }

        public int InfoMessageExistsXMLTo
        {
            get
            {
                return infoMessageExistsXMLTo;
            }

            set
            {
                infoMessageExistsXMLTo = value;
                NotifyPropertyChanged();
            }
        }

        public ObservableCollection<string> DefTranslate
        {
            get
            {
                return defTranslate;
            }

            set
            {
                defTranslate = value;
                NotifyPropertyChanged();
            }
        }

        public Translator()
        {
            TranslateFromLang = null;
            TranslateToLang = null;
            TranslateFromThisLang = null;
            TranslateToThisLang = null;
            InfoMessageTranslator = 0;
            InfoMessageExistsXMLFrom = 0;
            InfoMessageExistsXMLTo = 0;
            ExampleTranslate = new ObservableCollection<string>();
            DefTranslate = new ObservableCollection<string>();
        }
    }
}
