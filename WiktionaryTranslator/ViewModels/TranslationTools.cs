﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using WiktionaryTranslator.Models;
using WiktionaryTranslator.Resources;

namespace WiktionaryTranslator.ViewModels
{
    public class TranslationTools
    {
        public ObservableCollection<Language> initialyze(string path)
        {
            
            ObservableCollection<Language> Languages = new ObservableCollection<Language>();
            IEnumerable<XElement> entries = from x in XDocument.Load(path).Element("wiktionary").Elements("entry") select x;
            foreach (XElement entry in entries)
            {
                Language language = new Language(entry.Attribute("form").ToString().Replace("form=\"", "").Replace("\"", ""), new List<Definition>(), new List<string>(), new List<string>(), new List<Translation>());

                IEnumerable<XElement> lexemes = from x in entry.Elements("lexeme") select x;
                foreach (XElement lexeme in lexemes)
                {
                    IEnumerable<XElement> defs = from x in lexeme.Elements("defs") select x;
                    foreach (XElement def in defs)
                    {
                        IEnumerable<XElement> tlds = from x in def.Elements("toplevel-def") select x;
                        foreach (XElement tld in tlds)
                        {
                            language.Meaning.Add(new Definition(tld.Element("gloss") == null ? null : tld.Element("gloss").Value.ToString(), tld.Element("gloss") == null ? null : tld.Element("gloss").Attribute("domain") == null ? null : tld.Element("gloss").Attribute("domain").Value.ToString(), tld.Element("example") == null ? null : tld.Element("example").Value.ToString()));
                        }
                    }

                    IEnumerable<XElement> trans = from x in lexeme.Elements("trans") select x;
                    foreach (XElement tran in trans)
                    {
                        IEnumerable<XElement> items = from x in tran.Elements("item") select x;
                        foreach (XElement item in items)
                        {
                            language.Translate.Add(new Translation(item.Attribute("lang").Value.ToString(), item.Value.ToString()));
                        }
                    }

                    IEnumerable<XElement> syns = from x in lexeme.Elements("syn") select x;
                    foreach (XElement syn in syns)
                    {
                        IEnumerable<XElement> items = from x in syn.Elements("item") select x;
                        foreach (XElement item in items)
                        {
                            language.Synonym.Add(item.Value.ToString());
                        }
                    }

                    IEnumerable<XElement> ants = from x in lexeme.Elements("ant") select x;
                    foreach (XElement ant in ants)
                    {
                        IEnumerable<XElement> items = from x in ant.Elements("item") select x;
                        foreach (XElement item in items)
                        {
                            language.Antonym.Add(item.Value.ToString());
                        }
                    }
                }
                Languages.Add(language);
            }
            return Languages;
        }

        public string TranslateWord(string langCode, string word, ObservableCollection<Language> language)
        {
            string translation = null;

            if (Regex.IsMatch(word, @"^[a-zA-Z]+$"))
            {
                translation = (from pl in language where pl.Word.Equals(word.ToLower()) select (from plIn in pl.Translate where plIn.LangCode == langCode select plIn.TranslateWord).FirstOrDefault()).FirstOrDefault();

                if (translation != null)
                {
                    return translation;
                }
                else if (word.ToLower().EndsWith("ed"))
                {
                    translation = (from pl in language where pl.Word.Equals(word.ToLower().Substring(0, word.Length - 1)) select (from plIn in pl.Translate where plIn.LangCode == langCode select plIn.TranslateWord).FirstOrDefault()).FirstOrDefault();
                    if (translation != null)
                    {
                        return translation;
                    }
                    else
                    {
                        translation = (from pl in language where pl.Word.Equals(word.ToLower().Substring(0, word.Length - 2)) select (from plIn in pl.Translate where plIn.LangCode == langCode select plIn.TranslateWord).FirstOrDefault()).FirstOrDefault();
                        if (translation != null)
                        {
                            return translation;
                        }
                        else
                        {
                            return word;
                        }
                    }
                }
                else
                {
                    return word;
                }
            }
            else
            {
                return word;
            }
        }

        public string ExampleWord(string word, ObservableCollection<Language> language)
        {
            string translation = null;

            if (Regex.IsMatch(word, @"^[a-zA-Z]+$"))
            {
                translation = (from pl in language where pl.Word.Equals(word.ToLower()) select (from plIn in pl.Meaning select plIn.Example).FirstOrDefault()).FirstOrDefault();

                if (translation != null)
                {
                    return translation;
                }
                else if (word.ToLower().EndsWith("ed"))
                {
                    translation = (from pl in language where pl.Word.Equals(word.ToLower().Substring(0, word.Length - 1)) select (from plIn in pl.Meaning select plIn.Example).FirstOrDefault()).FirstOrDefault();
                    if (translation != null)
                    {
                        return translation;
                    }
                    else
                    {
                        translation = (from pl in language where pl.Word.Equals(word.ToLower().Substring(0, word.Length - 2)) select (from plIn in pl.Meaning select plIn.Example).FirstOrDefault()).FirstOrDefault();
                        if (translation != null)
                        {
                            return translation;
                        }
                        else
                        {
                            return word;
                        }
                    }
                }
                else
                {
                    return word;
                }
            }
            else
            {
                return word;
            }
        }

        public string DefWord(string word, ObservableCollection<Language> language)
        {
            string translation = null;

            if (Regex.IsMatch(word, @"^[a-zA-Z]+$"))
            {
                translation = (from pl in language where pl.Word.Equals(word.ToLower()) select (from plIn in pl.Meaning select plIn.Def).FirstOrDefault()).FirstOrDefault();

                if (translation != null)
                {
                    return translation;
                }
                else if (word.ToLower().EndsWith("ed"))
                {
                    translation = (from pl in language where pl.Word.Equals(word.ToLower().Substring(0, word.Length - 1)) select (from plIn in pl.Meaning select plIn.Def).FirstOrDefault()).FirstOrDefault();
                    if (translation != null)
                    {
                        return translation;
                    }
                    else
                    {
                        translation = (from pl in language where pl.Word.Equals(word.ToLower().Substring(0, word.Length - 2)) select (from plIn in pl.Meaning select plIn.Def).FirstOrDefault()).FirstOrDefault();
                        if (translation != null)
                        {
                            return translation;
                        }
                        else
                        {
                            return word;
                        }
                    }
                }
                else
                {
                    return word;
                }
            }
            else
            {
                return word;
            }
        }

        public string TranslateSentence(string langCode, string Sentence, ObservableCollection<Language> language)
        {
            if(Sentence == null)
            {
                return null;
            }

            char[] delimiterChars = { ' ', ',', '.', ':', '\t', '\n', '\'' }; // No time for handle correctly the ' character
            string[] words = Sentence.Split(delimiterChars);

            foreach (string s in words)
            {
                if (!s.Equals(""))
                {
                    int place = Sentence.LastIndexOf(s);
                    Sentence = Sentence.Remove(place, s.Length).Insert(place, TranslateWord(langCode, s, language)); // Problem raise here with ' in the sentence, don't know why it's not handle...
                }
            }

            if(Sentence.Length<=1)
                return Sentence.ToUpper();

            return char.ToUpper(Sentence[0]) + Sentence.Substring(1);
        }

        public ObservableCollection<string> ListOfExampleFromSentence(string Sentence, ObservableCollection<Language> language)
        {
            if (Sentence == null)
            {
                return null;
            }
            
            char[] delimiterChars = { ' ', ',', '.', ':', '\t', '\n', '\'' }; // No time for handle correctly the ' character
            string[] words = Sentence.Split(delimiterChars);

            ObservableCollection<string> list = new ObservableCollection<string>();

            foreach (string s in words)
            {
                if (!s.Equals(""))
                {
                    list.Add(ExampleWord(s, language));
                }
            }

            return list;
        }

        public ObservableCollection<string> ListOfDefFromSentence(string Sentence, ObservableCollection<Language> language)
        {
            if (Sentence == null)
            {
                return null;
            }
            
            char[] delimiterChars = { ' ', ',', '.', ':', '\t', '\n', '\'' }; // No time for handle correctly the ' character
            string[] words = Sentence.Split(delimiterChars);

            ObservableCollection<string> list = new ObservableCollection<string>();

            foreach (string s in words)
            {
                if (!s.Equals(""))
                {
                    list.Add(DefWord(s, language));
                }
            }

            return list;
        }
    }
}
