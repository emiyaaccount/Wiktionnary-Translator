﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using WiktionaryTranslator.Models;
using WiktionaryTranslator.Resources;

namespace WiktionaryTranslator.ViewModels
{
    public class LoadLanguages : NotifyProp
    { 
        private ICommand changeLangSelected;
        public ObservableCollection<Lang> LangCodeList { get; set; }
        public Lang LangSelected { get; set; }
        private DataString data;
        private Lang addLangSelected;
        private ICommand addWiktionaryXML;
        private ICommand changeAddLangSelected;

        public ICommand AddWiktionaryXML
        {
            get
            {

                return addWiktionaryXML = new RelayCommand<object>((obj) =>
                {
                    if (AddLangSelected == null)
                    {
                        MessageBox.Show(Data.LangMustBeSelected);
                        return;
                    }

                    OpenFileDialog openFileDialog = new OpenFileDialog();

                    openFileDialog.InitialDirectory = "c:\\";
                    openFileDialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                    openFileDialog.FilterIndex = 2;
                    openFileDialog.RestoreDirectory = true;

                    if (openFileDialog.ShowDialog() == true)
                    {
                        try
                        {
                            File.Copy(openFileDialog.FileName, Data.WiktionnaryXML + AddLangSelected.LangCode + "test" + Data.XML, true);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(Data.ErrReadDisk + ex.Message);
                        }
                    }

                    AddLangSelected = null;// new Lang("fr", "Français", "French");
                });
            }
        }

        public ICommand ChangeAddLangSelected
        {
            get
            {
                return changeAddLangSelected = new RelayCommand<Lang>((lang) =>
                {
                    AddLangSelected = lang;
                });
            }
        }

        public Lang AddLangSelected
        {
            get
            {
                return addLangSelected;
            }

            set
            {
                addLangSelected = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand ChangeLangSelected
        {
            get
            {
                return changeLangSelected = new RelayCommand<Lang>((primaryLanguage) =>
                {
                    LangSelected = primaryLanguage;

                    Data = new DataString();
                    
                    Data.LangMustBeSelected = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.LangMustBeSelected, MainViewModel.PrimaryLanguage);
                    Data.FileAdded = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.FileAdded, MainViewModel.PrimaryLanguage);
                    Data.ErrReadDisk = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.ErrReadDisk, MainViewModel.PrimaryLanguage);
                    Data.PrimaryLanguage = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.PrimaryLanguage, MainViewModel.PrimaryLanguage);
                    Data.AddWiktionary = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.AddWiktionary, MainViewModel.PrimaryLanguage);
                    Data.LangLibraryDoesntExist = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.LangLibraryDoesntExist, MainViewModel.PrimaryLanguage);
                    Data.Translation = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.Translation, MainViewModel.PrimaryLanguage);
                    Data.Definition = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.Definition, MainViewModel.PrimaryLanguage);
                    Data.Example = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.Example, MainViewModel.PrimaryLanguage);
                    Data.Synonym = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.Synonym, MainViewModel.PrimaryLanguage);
                    Data.Antonym = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.Antonym, MainViewModel.PrimaryLanguage);
                    Data.MessageInfoTranslator = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.MessageInfoTranslator, MainViewModel.PrimaryLanguage);
                    Data.MessageExistsXMLFrom = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.MessageExistsXMLFrom, MainViewModel.PrimaryLanguage);
                    Data.MessageExistsXMLTo = new TranslationTools().TranslateSentence(LangSelected.LangCode, Data.MessageExistsXMLTo, MainViewModel.PrimaryLanguage);
                });
            }
        }

        public DataString Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
                NotifyPropertyChanged();
            }
        }

        public LoadLanguages()
        {
            AddLangSelected = null;
            Data = new DataString();
            LangCodeList = new ObservableCollection<Lang>();

            StreamReader reader = new StreamReader(File.OpenRead(Data.LangCode));
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                LangCodeList.Add(new Lang(values[0], values[4], values[5]));
            }

            LangSelected = (from LangCodeL in LangCodeList where LangCodeL.LangCode == Data.DefaultLanguage select LangCodeL).First();
            
        }
    }
}
