﻿using System.Collections.ObjectModel;
using WiktionaryTranslator.Models;
using WiktionaryTranslator.Resources;

namespace WiktionaryTranslator.ViewModels
{
    public class MainViewModel
    {

        public LoadLanguages LoadLangue { get; set; }
        public Translator Translate { get; set; }
        public DisplayManager Displayer { get; set; }


        public static ObservableCollection<Language> PrimaryLanguage { get; set; } //static statement because the allocation of an ObservableCollection<Language> use a lot of cpu power and a lot of ram, and it's need to use it at any moment during the application's life, so it's just kept in memory
        public static string langCodeDefault { get; set; }

        public MainViewModel()
        {
            //TranslationTool = new TranslationTools();
            LoadLangue = new LoadLanguages();
            Translate = new Translator();
            Displayer = new DisplayManager();
            PrimaryLanguage = new ObservableCollection<Language>();
            PrimaryLanguage = new TranslationTools().initialyze(new DataString().PathPrimaryLanguage);
        }
    }
}
