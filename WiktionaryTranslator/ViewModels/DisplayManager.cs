﻿using System.Windows.Input;
using WiktionaryTranslator.Resources;

namespace WiktionaryTranslator.ViewModels
{
    public class DisplayManager : NotifyProp
    {
        private int translationSize;
        private string translationImageShowHide;
        private ICommand displayTranslation;

        private int exampleSize;
        private string exampleImageShowHide;
        private ICommand displayExample;

        private int defSize;
        private string defImageShowHide;
        private ICommand displayDef;

        public int TranslationSize
        {
            get
            {
                return translationSize;
            }

            set
            {
                translationSize = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand DisplayTranslation
        {
            get
            {
                return displayTranslation = new RelayCommand<object>((obj) =>
                {
                    if(TranslationSize == 60)
                    {
                        TranslationSize = 0;
                        TranslationImageShowHide = new DataString().ArrowDownBlackX1;
                    }
                    else
                    {
                        TranslationSize = 60;
                        TranslationImageShowHide = new DataString().ArrowUpBlackX1;
                    }
                });
            }
        }

        public string TranslationImageShowHide
        {
            get
            {
                return translationImageShowHide;
            }

            set
            {
                translationImageShowHide = value;
                NotifyPropertyChanged();
            }
        }

        public int ExampleSize
        {
            get
            {
                return exampleSize;
            }

            set
            {
                exampleSize = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand DisplayExample
        {
            get
            {
                return displayExample = new RelayCommand<object>((obj) =>
                {
                    if (ExampleSize == 60)
                    {
                        ExampleSize = 0;
                        ExampleImageShowHide = new DataString().ArrowDownBlackX1;
                    }
                    else
                    {
                        ExampleSize = 60;
                        ExampleImageShowHide = new DataString().ArrowUpBlackX1;
                    }
                });
            }
        }

        public string ExampleImageShowHide
        {
            get
            {
                return exampleImageShowHide;
            }

            set
            {
                exampleImageShowHide = value;
                NotifyPropertyChanged();
            }
        }

        public int DefSize
        {
            get
            {
                return defSize;
            }

            set
            {
                defSize = value;
                NotifyPropertyChanged();
            }
        }

        public string DefImageShowHide
        {
            get
            {
                return defImageShowHide;
            }

            set
            {
                defImageShowHide = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand DisplayDef
        {
            get
            {
                return displayDef = new RelayCommand<object>((obj) =>
                {
                    if (DefSize == 60)
                    {
                        DefSize = 0;
                        DefImageShowHide = new DataString().ArrowDownBlackX1;
                    }
                    else
                    {
                        DefSize = 60;
                        DefImageShowHide = new DataString().ArrowUpBlackX1;
                    }
                });
            }
        }

        public DisplayManager()
        {
            TranslationSize = 60;
            TranslationImageShowHide = new DataString().ArrowUpBlackX1;
            ExampleSize = 0;
            ExampleImageShowHide = new DataString().ArrowDownBlackX1;
            DefSize = 0;
            DefImageShowHide = new DataString().ArrowDownBlackX1;
        }
    }
}
