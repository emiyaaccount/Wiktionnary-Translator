﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WiktionaryTranslator.ViewModels
{
    public class NotifyProp : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged([CallerMemberName] string str = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }
    }
}
