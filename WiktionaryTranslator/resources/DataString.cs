﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WiktionaryTranslator.Resources
{
    public class DataString : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged([CallerMemberName] string str = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }
        /***** Path *****/
        public string RessourcePath { get; set; }
        public string LangCode { get; set; }
        public string WiktionnaryXML { get; set; }
        public string XML { get; set; }
        public string DefaultLanguage { get; set; }
        public string PathPrimaryLanguage { get; set; }
        public string ArrowDownBlackX1 { get; set; }
        public string ArrowUpBlackX1 { get; set; }
        public string Azur { get; set; }

        /***** Sentences & Words *****/
        public string LangMustBeSelected { get; set; }
        public string FileAdded { get; set; }
        public string ErrReadDisk { get; set; }
        private string primaryLanguage;
        private string addWiktionary;
        public string LangLibraryDoesntExist { get; set; }
        private string translation;
        private string definition;
        private string example;
        private string synonym;
        private string antonym;
        private string messageInfoTranslator;
        private string messageExistsXMLFrom;
        private string messageExistsXMLTo;

        public string PrimaryLanguage
        {
            get
            {
                return primaryLanguage;
            }

            set
            {
                primaryLanguage = value;
                NotifyPropertyChanged();
            }
        }

        public string AddWiktionary
        {
            get
            {
                return addWiktionary;
            }

            set
            {
                addWiktionary = value;
                NotifyPropertyChanged();
            }
        }

        public string Translation
        {
            get
            {
                return translation;
            }

            set
            {
                translation = value;
                NotifyPropertyChanged();
            }
        }

        public string Definition
        {
            get
            {
                return definition;
            }

            set
            {
                definition = value;
                NotifyPropertyChanged();
            }
        }

        public string Example
        {
            get
            {
                return example;
            }

            set
            {
                example = value;
                NotifyPropertyChanged();
            }
        }

        public string Synonym
        {
            get
            {
                return synonym;
            }

            set
            {
                synonym = value;
                NotifyPropertyChanged();
            }
        }

        public string Antonym
        {
            get
            {
                return antonym;
            }

            set
            {
                antonym = value;
                NotifyPropertyChanged();
            }
        }

        public string MessageInfoTranslator
        {
            get
            {
                return messageInfoTranslator;
            }

            set
            {
                messageInfoTranslator = value;
                NotifyPropertyChanged();
            }
        }

        public string MessageExistsXMLFrom
        {
            get
            {
                return messageExistsXMLFrom;
            }

            set
            {
                messageExistsXMLFrom = value;
                NotifyPropertyChanged();
            }
        }

        public string MessageExistsXMLTo
        {
            get
            {
                return messageExistsXMLTo;
            }

            set
            {
                messageExistsXMLTo = value;
                NotifyPropertyChanged();
            }
        }

        public DataString()
        {
            /***** Path *****/
            RessourcePath = @"ExternalResources\";
            LangCode = RessourcePath + @"Languages\langCode.csv";
            WiktionnaryXML = RessourcePath + @"WiktionnaryXML\";
            XML = ".xml";
            DefaultLanguage = "en";
            PathPrimaryLanguage = WiktionnaryXML + DefaultLanguage + XML;
            ArrowDownBlackX1 = "../Resources/Fonts/ic_keyboard_arrow_down_black_24dp_1x.png";
            ArrowUpBlackX1 = "../Resources/Fonts/ic_keyboard_arrow_up_black_24dp_1x.png";
            Azur = "#1E7FCB";

            /***** Sentences & Words *****/
            LangMustBeSelected = "The language name must be selected ! ";
            FileAdded = "File Added !";
            ErrReadDisk = "Error: Could not read file from disk. Original error: ";
            PrimaryLanguage = "Primary language : ";
            AddWiktionary = "Add Wiktionnary.xml";
            LangLibraryDoesntExist = "Could not load this language, you must first add it";
            Translation = "Translation";
            Definition = "Definition";
            Example = "Example";
            Synonym = "Synonym";
            Antonym = "Antonym";
            MessageInfoTranslator = "Error, you must select the language from which translate and to translate \nand at least the xml file of the language from which translate";
            MessageExistsXMLFrom = "Error, you must add the file .xml of the language from which you want to translate";
            MessageExistsXMLTo = "Error, you must add the file .xml of the language to which you want to translate";
        }
    }
}
